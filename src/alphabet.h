/*
 * =====================================================================================
 *
 *       Filename:  alphabet.h
 *
 *         Author:  Biel Sala (B), bielsalamimo@gmail.com
 *
 * =====================================================================================
 */

namespace alphabet {
    const std::string letters   = "qwertyuiopasdfghjklzxcvbnm" /* letter only */
                                  "QWERTYUIOPASDFGHJKLZXCVBNM";


    const std::string letters_lowercase = "qwertyuiopasdfghjklzxcvbnm"; /* lowercase */


    const std::string letters_uppercase = "QWERTYUIOPASDFGHJKLZXCVBNM"; /* uppercase */


    const std::string alphanumeric  = "qwertyuiopasdfghjklzxcvbnm" /* alphanumeric */
                                      "QWERTYUIOPASDFGHJKLZXCVBNM"
                                      "1234567890_";


    const std::string all   = "qwertyuiopasdfghjklzxcvbnm" /* everything */
                              "QWERTYUIOPASDFGHJKLZXCVBNM"
                              "1234567890!\"$%&/()=<>.-_'*+";
}
