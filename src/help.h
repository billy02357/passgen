/*
 * =====================================================================================
 *
 *       Filename:  help.h
 *
 *         Author:  Biel Sala (B), bielsalamimo@gmail.com
 *
 * =====================================================================================
 */
#include <iostream>

void printhelp(void) {
       using namespace std;

       cerr << ""
              "Usage: passgen [length] [amount|option] [option]\n\n"
              "Options:\n"
              "\t-a\t\tGenerate password(s) with alphanumeric characters\n"
              "\t-l\t\tGenerate password(s) with lowercase/uppercase letters only\n"
              "\t-ll\t\tGenerate password(s) with lowercase letters only\n"
              "\t-lu\t\tGenerate password(s) with upwercase letters only\n"
              "\tno option\tGenerate password(s) with alphanumeric characters and symbols\n"
              "\a";
       exit(EXIT_FAILURE);
}
