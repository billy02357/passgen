/*
 * =====================================================================================
 *
 *       Filename:  main.cpp
 *
 *       Description:  Password generator
 *
 *       Version:  1.0.2
 *       Created:  08/13/2021 05:18:19 PM
 *       Revision:  none
 *       Compiler:  g++
 *
 *       Author:  Biel Sala (B), bielsalamimo@gmail.com
 *       Organization:
 *
 * =====================================================================================
 */
#include <iostream>
#include <stdlib.h>
#include <string>
#include <cstring>
#include "alphabet.h"
#include "getrandom.h"
#include "help.h"

int amount = 1;


int getrandom(void); /* Gets a random number from milliseconds (sys/time.h) */
void printhelp(void); /* Prints help message and exits */

int main(int argc, char *argv[]) {
       if (argc == 1 || !strcmp(argv[1], "-h")) /* Print help if there is only one argument or the second argument is "-h" */
              printhelp();


       if (argc == 3) { /* If there are three arguments, first must be the length, so change the value of alphabet::alphabet_type based on the argv[2] value */
              if (!strcmp(argv[2], "-a")) {
                     alphabet::alphabet_type = alphabet::ALPH;

              } else if (!strcmp(argv[2], "-l")) {
                     alphabet::alphabet_type = alphabet::LET;

              } else if (!strcmp(argv[2], "-ll")) {
                     alphabet::alphabet_type = alphabet::LET_L;

              } else if (!strcmp(argv[2], "-lu")) {
                     alphabet::alphabet_type = alphabet::LET_U;

              } else {
                     amount = atoi(argv[2]); /* If the second argument is not an option, it means the user only specified an amount of passwords  */
              }

       } else if (argc == 4) { /* Same as above but there is option and amount specified */
              if (!strcmp(argv[3], "-a")) {
                     alphabet::alphabet_type = alphabet::ALPH;
                     amount = atoi(argv[2]);

              } else if (!strcmp(argv[3], "-l")) {
                     alphabet::alphabet_type = alphabet::LET;
                     amount = atoi(argv[2]);

              } else if (!strcmp(argv[3], "-ll")) {
                     alphabet::alphabet_type = alphabet::LET_L;
                     amount = atoi(argv[2]);

              } else if (!strcmp(argv[3], "-lu")) {
                     alphabet::alphabet_type = alphabet::LET_U;
                     amount = atoi(argv[2]);
              }

       }


       for (int a = 0; a < amount; a++) { /* Generate the passwords based on alphabet::alphabet_type */
              switch (alphabet::alphabet_type) {
              case alphabet::ALL:
                     for (int l = 0; l < atoi(argv[1]); l++) {
                            std::cout << alphabet::all[getrandom()];
                     }
                     break;

              case alphabet::ALPH:
                     for (int l = 0; l < atoi(argv[1]); l++) {
                            std::cout << alphabet::alphanumeric[getrandom()];
                     }
                     break;

              case alphabet::LET:
                     for (int l = 0; l < atoi(argv[1]); l++) {
                            std::cout << alphabet::letters[getrandom()];
                     }
                     break;

              case alphabet::LET_L:
                     for (int l = 0; l < atoi(argv[1]); l++) {
                            std::cout << alphabet::letters_lowercase[getrandom()];
                     }
                     break;

              case alphabet::LET_U:
                     for (int l = 0; l < atoi(argv[1]); l++) {
                            std::cout << alphabet::letters_uppercase[getrandom()];
                     }
                     break;

              default:
                     return EXIT_FAILURE; /* If none of the above are matched, there's been some kind of error involving alphabet::alphabet_type */
              }

              std::cout << "\n";

       }

       return EXIT_SUCCESS;
}
