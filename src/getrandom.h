
/*
 * =====================================================================================
 *
 *       Filename:  getrandom.h
 *
 *       Author:  Biel Sala (B), bielsalamimo@gmail.com
 *
 * =====================================================================================
 */
#include <sys/time.h>

namespace alphabet {
    enum dicttype { ALPH, LET, LET_L, LET_U, ALL };
    int alphabet_type = alphabet::ALL; /* program will change this value based on arguments */
}

int getrandom() {
       struct timeval t;
       gettimeofday(&t, nullptr);
       srand(t.tv_usec * t.tv_sec); /* Get seed for rand() from gettimeofday() */

       /* Depending on the value of alphabet_type (based on arguments) rand() gets a random number in the range of the different "alphabets" */
       switch (alphabet::alphabet_type) {
       case alphabet::ALL:
              return rand() % (alphabet::all.length()+1); /* "+1" is because rand() does not take the max range number as included */

       case alphabet::ALPH:
              return rand() % (alphabet::alphanumeric.length()+1);

       case alphabet::LET:
              return rand() % (alphabet::letters.length()+1);

       case alphabet::LET_L:
              return rand() % (alphabet::letters_lowercase.length()+1);

       case alphabet::LET_U:
              return rand() % (alphabet::letters_uppercase.length()+1);

       default:
              return 1;

       }

}


