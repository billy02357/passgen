MANPREFIX = /usr/share/man

passgen:
	@ninja

install: passgen
	@mkdir -p $(DESTDIR)$(PREFIX)/usr/bin
	@/bin/cp -vf passgen $(DESTDIR)$(PREFIX)/usr/bin
	@chmod 755 $(DESTDIR)$(PREFIX)/usr/bin/passgen
	@echo Installing manual page to $(MANPREFIX)/man1
	@mkdir -p $(MANPREFIX)/man1
	@gzip -f passgen.1 -k
	@/bin/cp -vf passgen.1.gz $(MANPREFIX)/man1/passgen.1.gz
	@chmod 644 $(MANPREFIX)/man1/passgen.1.gz

clean:
	@rm -f passgen
	@rm -f passgen.1.gz

uninstall:
	@rm -f $(DESTDIR)$(PREFIX)/usr/bin/passgen
	@rm -f $(MANPREFIX)/man1/passgen.1.gz
