# passgen
Password generator in C++ using `<sys/time.h>`'s `gettimeofday()` to generate random numbers

## Usage
See `passgen.1` or `man passgen` after installing

## Building/Installing

You can use the `make` command with the options `install`, `uninstall` and `clean`

Example:
```
make install
```
will install the binary in `/usr/bin/passgen` and install the manual page in `/usr/share/man/man1/passgen.1.gz`

Run `ninja` for building the program
